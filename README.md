This is a demo of how you can know the "current directory" in a `source`d shell
script. However that's a bit of a vague term because is "current" refering to
the current directory from where the original script was *called from* or where
the current script itself *lives*. Those can be two different paths.

# What this demo shows
Using `$(dirname "$0")` will give you the current directory from where the
script was *called*. And if you're deep into a chain of `source`d files, the
path is related to the original script that was `source`d, *not* the one you're
currently in.

Using `$(dirname "${BASH_SOURCE[0]}")` will give you the current directory of
the script you're executing *inside*. Even when you have the chain of `source`
commands, it'll reflect the current file's path.

# How to run
You need a computer with a shell, like `bash` or `zsh`.

1. clone this repo
1. run the `foo/one.sh` script from a few different paths and note how the
   output changes. Here some ways you might want to call it
   ```bash
   cd foo; sh ./one.sh
   cd ..; sh ./shell-script-current-dir-demo/foo/one.sh
   sh /full/path/to/shell-script-current-dir-demo/foo/one.sh
   ```

# Example output
So you don't have to run the script, here are the results.

## Running *in* the `foo` dir
```bash
$ sh ./one.sh
one bs=    one.sh
one dnbs=  .
one dn=    .
two bs=    ./two.sh
two dnbs=  .
two dn=    .
three bs=  ./../blah/three.sh
three dnbs=./../blah
three dn=  .
four bs=   ./../blah/four.sh
four dnbs= ./../blah
four dn=   .
```

## Running from outside the git repo with relative path
```bash
$ sh ./shell-script-current-dir-demo/foo/one.sh
one bs=    ./shell-script-current-dir-demo/foo/one.sh
one dnbs=  ./shell-script-current-dir-demo/foo
one dn=    ./shell-script-current-dir-demo/foo
two bs=    ./shell-script-current-dir-demo/foo/two.sh
two dnbs=  ./shell-script-current-dir-demo/foo
two dn=    ./shell-script-current-dir-demo/foo
three bs=  ./shell-script-current-dir-demo/foo/../blah/three.sh
three dnbs=./shell-script-current-dir-demo/foo/../blah
three dn=  ./shell-script-current-dir-demo/foo
four bs=   ./shell-script-current-dir-demo/foo/../blah/four.sh
four dnbs= ./shell-script-current-dir-demo/foo/../blah
four dn=   ./shell-script-current-dir-demo/foo
```

## Running with a fully qualified path
```bash
$ sh /zeta/git/shell-script-current-dir-demo/foo/one.sh
one bs=    /zeta/git/shell-script-current-dir-demo/foo/one.sh
one dnbs=  /zeta/git/shell-script-current-dir-demo/foo
one dn=    /zeta/git/shell-script-current-dir-demo/foo
two bs=    /zeta/git/shell-script-current-dir-demo/foo/two.sh
two dnbs=  /zeta/git/shell-script-current-dir-demo/foo
two dn=    /zeta/git/shell-script-current-dir-demo/foo
three bs=  /zeta/git/shell-script-current-dir-demo/foo/../blah/three.sh
three dnbs=/zeta/git/shell-script-current-dir-demo/foo/../blah
three dn=  /zeta/git/shell-script-current-dir-demo/foo
four bs=   /zeta/git/shell-script-current-dir-demo/foo/../blah/four.sh
four dnbs= /zeta/git/shell-script-current-dir-demo/foo/../blah
four dn=   /zeta/git/shell-script-current-dir-demo/foo
```
